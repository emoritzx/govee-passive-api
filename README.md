# govee-passive-api

Get certain types of Govee device information via REST API calls

This repo is intended as a reference model for use in https://github.com/emoritzx/hacs-govee-passive

## Why?

The documented API does not include all types of devices.
There is a separate API that provides this information.
