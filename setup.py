from setuptools import setup

setup(
    name="Govee Passive API",
    version="1.0",
    install_requires=[
        "requests",
        "pyjwt",
    ]
)
