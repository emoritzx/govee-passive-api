#!/bin/bash -eu

# Example of making the REST calls to the undocumented API

LOGIN_ENDPOINT="https://app2.govee.com/account/rest/account/v1/login"
DEVICE_ENDPOINT="https://app2.govee.com/device/rest/devices/v1/list"

function generate_client_id {
    local username="$1"
    echo "curl-$(base64 <<< "${username}")-${RANDOM}"
}

function login {
    local client_id="$1"
    local username="$2"
    local password="$3"
    curl -s \
        -X POST \
        -H 'Content-Type: application/json' \
        "${LOGIN_ENDPOINT}" \
        -d "{\"email\":\"${username}\",\"password\":\"${password}\",\"client\":\"${client_id}\"}"
}

function extract_token {
    if [[ "$1" =~ \"token\":\"(.+)\" ]]; then
        cut -f1 -d'"' <<< "${BASH_REMATCH[1]}"
    else
        echo "ERROR: Failed to extract login token." >&2
    fi
}

function get_devices {
    local client_id="$1"
    local token="$2"
    curl -s -w "\\n" \
        -X POST \
        -H "Authorization: Bearer ${token}" \
        "${DEVICE_ENDPOINT}"
}

function main {
    local username="$1"
    local password="$2"
    local client_id="$(generate_client_id "${username}")"
    local data="$(login "${client_id}" "${username}" "${password}")"
    local token="$(extract_token "${data}")"
    get_devices "${client_id}" "${token}"
}

if [[ "$(basename -- "$0")" == "$(basename -- "${BASH_SOURCE}")" ]]; then
    main "$@"
fi
