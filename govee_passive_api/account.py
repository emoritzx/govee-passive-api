import json
import jwt

from datetime import datetime
from typing import IO, Any

from . import auth

class Account:
    
    def __init__(self, data):
        self._data = data

    @property
    def client_id(self) -> str:
        return self._data["client"]

    @property
    def expired(self) -> bool:
        raw_token = self.token
        token = jwt.decode(raw_token, options={"verify_signature": False})
        expiration_timestamp = token["exp"]
        expiration_time = datetime.fromtimestamp(expiration_timestamp)
        return expiration_time < datetime.now()

    @property
    def token(self) -> str:
        return self._data["token"]

    def export(self, fp: IO[Any]):
        json.dump(self._data, fp)
    
def from_basic(username: str, password: str, client_id: str = None) -> Account:
    login_data = auth.login(username, password, client_id)
    return Account(login_data)

def from_saved(data: dict) -> Account:
    return Account(data)

def from_file(fp: IO[Any]) -> Account:
    data = json.load(fp)
    return from_saved(data)
