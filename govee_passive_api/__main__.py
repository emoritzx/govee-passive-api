import argparse
import json
import logging
import pathlib
import sys

from . import account
from .api import Govee

parser = argparse.ArgumentParser()
parser.add_argument("--username")
parser.add_argument("--password")
parser.add_argument("--load", type=pathlib.Path)
parser.add_argument("--save", type=pathlib.Path)
parser.add_argument("--log-level", default="INFO")

if __name__ == "__main__":

    args = parser.parse_args()
    logging.basicConfig(level=args.log_level)

    if args.load:
        with open(args.load, 'r') as saved_file:
            account_info = account.from_file(saved_file)
        if account_info.expired:
            raise Exception("Login session has expired, you must login again with credentials.")

    elif args.username and args.password:
        account_info = account.from_basic(args.username, args.password)

    else:
        raise Exception("You must supply account login credentials")

    if args.save:
        with open(args.save, 'w') as save_file:
            account_info.export(save_file)

    api = Govee(account_info)
    devices = api.get_devices()
    json.dump(devices, sys.stdout, default=vars)
