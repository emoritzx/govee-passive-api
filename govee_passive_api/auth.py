import requests
from requests.auth import AuthBase
import uuid

LOGIN_ENDPOINT = "https://app2.govee.com/account/rest/account/v1/login"

class BearerToken(AuthBase):
    """
        Use Bearer authorization
        See: https://stackoverflow.com/a/58055668/2304254
    """
    def __init__(self, token: str):
        self.token = token

    def __call__(self, request):
        request.headers["authorization"] = f"Bearer {self.token}"
        return request

def login(username: str, password: str, client_id: str = None):
    client_id = client_id \
        if client_id \
        else _generate_client_id()
    data = {
        "email": username,
        "password": password,
        "client": client_id
    }
    response = requests.post(LOGIN_ENDPOINT, json=data)
    response.raise_for_status()
    response_data = response.json()

    # check data response code
    # I dunno why it's different than the HTTP response code 🙄
    inner_status_code = response_data["status"]
    if inner_status_code != 200:
        raise Exception(f"Error logging in ({inner_status_code}): {response_data['message']}")

    client_data = response_data["client"]
    return client_data

def _generate_client_id() -> str:
    return str(uuid.uuid4())
