import json
import requests

from .account import Account
from .auth import BearerToken

DEVICE_ENDPOINT = "https://app2.govee.com/device/rest/devices/v1/list"

class Govee:

    def __init__(self, account: Account):
        self.account = account

    def get_devices(self) -> list[object]:
        response = requests.post(DEVICE_ENDPOINT, auth=BearerToken(self.account.token))
        response.raise_for_status()
        response_data = response.json()

        inner_status_code = response_data["status"]
        if inner_status_code != 200:
            raise Exception(f"Error getting devices ({inner_status_code}): {response_data['message']}")
        
        raw_devices = response_data["devices"]
        return [ Device(raw_device) for raw_device in raw_devices ]


class Device:

    def __init__(self, data: dict):
        self.deviceExt = {}
        for key, value in data.items():
            if key == 'deviceExt':
                for extra_key, extra_value in value.items():
                    self.deviceExt[extra_key] = json.loads(extra_value)
            else:
                setattr(self, key, value)
